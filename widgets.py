import datetime as dt
import calendar
from wtforms.widgets import HTMLString, html_params

class CalendarWidget(calendar.Calendar):

    def picker_button(self, field, date):
        return '<form method="POST"><button name="%s" value="%s">%s</button></form>' % (field.name, date, date.day)

    def __call__(self, field, **kwargs):
        html = []
        # TODO: show week number
        #       highlight current
        #       highlighted buttons:hover
        #
        a = html.append

        a('<table %s>' % html_params(**kwargs))

        a('<caption>')

        a('<form method="GET">')

        a('<select name="month" onchange="this.form.submit()">')

        for monthindex in xrange(1, 13):
            a('<option value="%s"' % monthindex)
            if monthindex == field.data.month:
                a(' selected ')
            a('>%s</option>' % calendar.month_name[monthindex])

        a('</select><noscript><button type="submit">change</button></noscript>')

        a('<input name="year" type="number" value="%s" onchange="this.form.submit()">' % field.data.year)
        a('</form>')

        a('</caption>')

        a('<thead><tr>')
        for weekindex in self.iterweekdays():
            a('<th>%s</th>' % calendar.day_abbr[weekindex])
        a('</tr></thead>')

        a('<tbody>')
        for week in self.monthdatescalendar(field.data.year, field.data.month):
            a('<tr>')
            for date in week:
                a('<td')
                c = []
                if date == field.data:
                    print date
                    print field.data
                    #
                    #c.append('current')
                if date == dt.date.today():
                    c.append('today')
                if c:
                    a(' class="%s"' % ' '.join(c))

                if date.month == field.data.month:
                    a('>%s</td>' % self.picker_button(field, date))
                else:
                    a('>%s</td>' % date.day)

            a('</tr>')
        a('</tbody>')

        a('</table>')
        return HTMLString(''.join(html))
