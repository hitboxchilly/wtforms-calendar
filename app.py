import datetime as dt
import calendar

from flask import Flask, current_app, redirect, render_template_string, request, url_for
from wtforms import Form, DateField

from widgets import CalendarWidget

app = Flask(__name__)

class CalendarForm(Form):
    date = DateField(default=dt.date.today, widget=CalendarWidget(firstweekday=1))


class database:
    date = dt.date(2016,10,31)

TEMPLATE = '''\
<html>
    <head>
        <title>Calendar</title>
        <style>
        body {
            margin:2em auto;
            width: 500px;
        }
        form {
            margin:0;
            padding:0;
        }
        table {
            border-collapse: collapse;
        }
        th {
            color:#666;
        }
        td,th {
            border: 1px solid #ccc;
        }
        td {
            font-size: 1em;
            text-align: center;
            vertical-align: middle;
            padding:0;
            color:#ccc;
        }
        input, select {
            text-align: center;
            height: 1.75em;
            margin: .5em .25em;
        }
        input[name="year"] {
            width: 6em;
        }
        button {
            margin:0
            font-size: 1em;
            width: 4em;
            height: 4em;
            background: transparent;
            border: none;
        }
        button:hover {
            border: none;
        }
        button:hover {
            cursor: pointer;
            background: #333;
            color: #fff;
        }
        td.today button {
            color: #fff;
            background: #e95e00;
        }
        td.current button {
            color: #fff;
            background: #ff0000;
        }
        td.today.current button {
            font-weight: bold;
        }
        </style>
    </head>
    <body>
        <div><span>form.date.data </span>{{ form.date.data }}</div>
        <div><span>database.date </span>{{ database.date }}</div>
        <div>{{ form.date() }}</div>
    </body>
</html>
'''

@app.route('/', methods=['GET', 'POST'])
def index():
    form = CalendarForm(request.form, date=database.date)

    if request.method == 'POST' and form.validate():
        database.date = form.date.data
        return redirect(url_for('index'))

    year = int(request.args.get('year', form.date.data.year))
    month = int(request.args.get('month', form.date.data.month))

    if year != form.date.data.year or month != form.date.data.month:
        # update form's date and resolve an invalid day value
        day = form.date.data.day
        while True:
            try:
                form.date.data = form.date.data.replace(year, month, day)
            except ValueError:
                day -= 1
            else:
                break

    return render_template_string(TEMPLATE, form=form, database=database)
